
class Flat:
    address =""
    rooms = 0
    size = 0
    price = 0
    def __init__(self, id, wbs, flat_link):
        self.id = id
        self.wbs = wbs
        self.flat_link = flat_link

    def __str__(self):
        wbs_req = ""
        if self.wbs == 0:
            wbs_req = "No WBS needed"
        else:
            wbs_req = "WBS needed"
        return f"id: {self.id}, rooms: {self.rooms}, size: {self.size}, price: {self.price}, {wbs_req}, Address: {self.address}, Link: {self.flat_link}"

class Inidicator:
    update_period = 0

    def __init__(self, fetched_time):
        self.fetched_time = fetched_time
    
    def __str__(self):
        return f"Market indicator that should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class CrackSpread(Inidicator):
    value = 0
    name_str = "crack spread"
    update_period = 24  #check every 24 hours for a new one 
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class FFRate(Inidicator):
    value = 0
    name_str = "federal fund rate"
    update_period = 24*31  #check every month for a new one
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class FertilityRate(Inidicator):
    value = 0
    name_str = "fertility rate"
    update_period = 24*31*365  #check every year for a new one
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class Libor(Inidicator):
    value = 0
    name_str = "Libor"
    update_period = 24  #check every day
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class M2MoneySupply(Inidicator):
    value = 0
    name_str = "M2 Money Supply"
    update_period = 24*31 #check every month
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class newHomesSales(Inidicator):
    value = 0
    name_str = "new Home Sales"
    update_period = 24*31 #check every month
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"

class realInterestRate(Inidicator):
    value = 0
    name_str = "real interest rate"
    update_period = 24*31 #check every month
    def __init__(self, fetched_time):
        super().__init__(fetched_time)
    
    def __str__(self):
        return f"Indicator: {self.name_str}, value: {self.value} should be checked every {self.update_period} hours, last time it has been checked: {self.fetched_time}"
from flask import Flask
import os
import threading
from datetime import datetime
from datetime import date
from contextlib import redirect_stdout

from views import views
from classes import *
from flatparsing import flatParsingTask
from indicatorparsing import indicatorParsingTask

app = Flask(__name__)
app.register_blueprint(views, url_prefix="/")

def FlaskTask():
    app.run(debug=False, host='0.0.0.0')

if __name__ == '__main__':
    
    print("Running...All output forwarded to log files.txt")
    now = datetime.now()
    today = date.today()
    current_time = now.strftime("%H:%M:%S")
    time_for_log = str(today) + " on " + str(current_time)
    welcome_msg = "Program started on "+ time_for_log
    with open('flat_bot_log.txt', 'w') as f:
        with redirect_stdout(f):
            print(welcome_msg)
    
    with open('indicator_bot_log.txt', 'w') as f:
        with redirect_stdout(f):
            print(welcome_msg)
    
    #create threads
    flatParseTask = threading.Thread(target=flatParsingTask, name='flatParsingTask')
    webServerTask = threading.Thread(target=FlaskTask, name='flaskServerTask')
    indicatorParseTask = threading.Thread(target=indicatorParsingTask, name='indicatorParsingTask')

    webServerTask.start()
    flatParseTask.start()
    indicatorParseTask.start()

    #wait unil all threads finish (should never happen)
    webServerTask.join()
    flatParseTask.join()    
    indicatorParseTask.join()

    #flat_var = Flat(34, 0, "rreeer.htm")
    #print(f"flat variabel is of type {type(flat_var)}, with this data id: {flat_var.id}, {flat_var.wbs}, {flat_var.flat_link}")
    #app.run(debug=True, host='0.0.0.0')
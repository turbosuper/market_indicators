from contextlib import redirect_stdout
from datetime import datetime
from datetime import date
import time
import requests
from bs4 import BeautifulSoup
#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.common.by import By
#from webdriver_manager.chrome import ChromeDriverManager
import schedule
import json

from classes import *


crackspread_list = []
ffrate_list = []
fertility_rate_list = []
libor_list = []
m2MS_list = []
new_homes_sales_list= []
real_interest_rate_list = []

def indicatorParsingTask():

    #get some data- so that the webpage doesn't look empty on the first run
    crackspread_210223 = CrackSpread("21/02/2023 15:04")       
    crackspread_210223.value = 26.231 
    crackspread_170223 = CrackSpread("17/02/2023 15:04")      
    crackspread_170223.value = 25.984
    crackspread_160223 = CrackSpread("16/02/2023 15:04")       
    crackspread_160223.value = 26.478 
    crackspread_list.append(crackspread_160223)
    crackspread_list.append(crackspread_170223)
    crackspread_list.append(crackspread_210223)
    getCrackSpread(crackspread_list)
    print(f"CS indicators len is now: {len(crackspread_list)}")
    for i in range(len(crackspread_list)):
        print(f"Value of the {i}th save crack spread is {crackspread_list[i].value} and it has been fetched on {crackspread_list[i].fetched_time}.")
    
    libor_170222 = Libor("17/02/2023 13:44")
    libor_170222.value = 4.59129
    libor_160222 = Libor("16/02/2023 13:44")
    libor_160222.value = 4.59786
    libor_150222 = Libor("15/02/2023 13:44")
    libor_150222.value = 4.60143
    libor_list.append(libor_150222)
    libor_list.append(libor_160222)
    libor_list.append(libor_170222)
    getLibor(libor_list)

    m2MS_112022 = M2MoneySupply("13/11/2022 13:44")
    m2MS_112022.value = 21354.8
    m2MS_102022 = M2MoneySupply("13/10/2022 13:44")
    m2MS_102022.value = 21415.5
    m2MS_092022 = M2MoneySupply("13/09/2022 13:44")
    m2MS_092022.value = 21503.3
    m2MS_list.append(m2MS_092022)
    m2MS_list.append(m2MS_102022)
    m2MS_list.append(m2MS_112022)
    getM2MS(m2MS_list)

    newHS_112022 = newHomesSales("14/11/2022 12:44")
    newHS_112022.value = 602
    newHS_102022 = newHomesSales("14/10/2022 12:44")
    newHS_102022.value = 598
    newHS_092022 = newHomesSales("14/09/2022 12:44")
    newHS_092022.value = 550
    new_homes_sales_list.append(newHS_092022)
    new_homes_sales_list.append(newHS_102022)
    new_homes_sales_list.append(newHS_112022)
    getNewHomesSales(new_homes_sales_list)

    real_interest_012023 = realInterestRate("15/01/2023 11:44")
    real_interest_012023.value = 1.7831
    real_interest_122022 = realInterestRate("15/12/2022 11:44")
    real_interest_122022.value = 1.57613
    real_interest_112022 = realInterestRate("15/11/2022 11:44")
    real_interest_112022.value = 1.93071
    real_interest_rate_list.append(real_interest_112022)
    real_interest_rate_list.append(real_interest_122022)
    real_interest_rate_list.append(real_interest_012023)
    getRealInterestRate(real_interest_rate_list)

    ffrate_1222 = FFRate("20/12/2022 13:44")        #get some old values first
    ffrate_1222.value = 4.10 
    ffrate_1122 = FFRate("17/11/2022 15:44")
    ffrate_1122.value = 3.78
    ffrate_1022 = FFRate("17/10/2022 15:44")
    ffrate_1022.value = 3.08
    ffrate_list.append(ffrate_1022) 
    ffrate_list.append(ffrate_1122) 
    ffrate_list.append(ffrate_1222) 
    getfederalFundRate(ffrate_list)
    print(f"FFR indicators len is now: {len(ffrate_list)}")
    for i in range(len(ffrate_list)):
        print(f"Value of the {i}th ffrate is {ffrate_list[i].value} and it has been fetched on {ffrate_list[i].fetched_time}.")

    fffrate_2019 = FertilityRate("09/08/2019 12:44")
    fffrate_2019.value = 1.706
    fffrate_2018 = FertilityRate("09/08/2018 12:44")
    fffrate_2018.value = 1.7295
    fffrate_2017 = FertilityRate("09/08/2017 12:44")
    fffrate_2017.value = 1.7655
    fertility_rate_list.append(fffrate_2017)
    fertility_rate_list.append(fffrate_2018)
    fertility_rate_list.append(fffrate_2019)
    getFertilityRate(fertility_rate_list)
    
    #schedule all the functions to run periodically
    schedule.every(2).minutes.do(getCrackSpread, crackspread_list)      #daily
    schedule.every(2).minutes.do(getLibor,libor_list)                   #daily
    schedule.every(2).minutes.do(getM2MS, m2MS_list)                            #monthly
    schedule.every(3).minutes.do(getNewHomesSales, new_homes_sales_list)        #monthly
    schedule.every(4).minutes.do(getRealInterestRate,real_interest_rate_list)   #monthly
    schedule.every(2).minutes.do(getfederalFundRate,ffrate_list)                #monthly    
    schedule.every(2).minutes.do(getFertilityRate,fertility_rate_list)         #annualy

    #production setup
    #schedule.every().day.at("9:30").do(getCrackSpread, crackspread_list)      #daily
    #schedule.every().day.at("9:30").do(getLibor,libor_list)                   #daily
    #schedule.every(4).weeks.do(getM2MS, m2MS_list)                            #monthly
    #schedule.every(4).weeks.do(getNewHomesSales, new_homes_sales_list)        #monthly
    #schedule.every(4).weeks.do(getRealInterestRate,real_interest_rate_list)   #monthly
    #schedule.every(4).weeks.do(getfederalFundRate,ffrate_list)                #monthly    
    #schedule.every(52).weeks.do(getFertilityRate,fertility_rate_list)         #annualy
    while True:
        schedule.run_pending()
        time.sleep(1)
    
    #scheduler = sched.scheduler(time.time, time.sleep)
    #csEvent = scheduler.enter(150,3,getCrackSpread,(crackspread_list,))
    #liborEvent = scheduler.enter(30,4,getLibor,(libor_list,))
    #m2msEvent = scheduler.enter(30,5,getM2MS,(m2MS_list,))
    #newhomesEvent = scheduler.enter(45,6,getNewHomesSales,(new_homes_sales_list,))
    #realInterestEvent = scheduler.enter(45,7,getRealInterestRate,(real_interest_rate_list,))
    #ffrEvent = scheduler.enter(45,8,getfederalFundRate,(ffrate_list,))
    #fertilityEvent = scheduler.enter(45,9,getFertilityRate,(fertility_rate_list,))
    #scheduler.run()
    
    #current_CrackSpread = CrackSpread(23, "01.02.03")
    #current_CrackSpread2 = CrackSpread(645, "33.66.44")
    #print(current_CrackSpread)
    #crackspread_list.append(current_CrackSpread2)
    #all_indicators.append(current_CrackSpread)
    #print(f"all indicators[0] now: {print(crackspread_list[0])}")
    #print(f"all indicators[1] now: {print(crackspread_list[1])}")
    #with open('indicator_bot_log.txt', 'a') as f:
    #    with redirect_stdout(f):
    #        print(f"all indicators now: {print(all_indicators)}")

#function that fetches the crack spread object, and appends it to the list
def getCrackSpread(crack_spread_obj_list):
    current_crack_spread_obj = parseForCrackSpread()
    crack_spread_obj_list.append(current_crack_spread_obj)
    print(f"New value appended. The crack spread obj list is now {len(crack_spread_obj_list)} elem long.")

def parseForCrackSpread():
    URL = "https://www.barchart.com/futures/quotes/IGO*1"
    #website blocks requests, need to try harder
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36'}
    #try 3 times the parsing:                
    try:
        page = requests.get(URL, headers=headers)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the crack spread failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the crack spread failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the crack spread failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find(id="main-content-column")
    #with open('parsed_webpage.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(results.prettify())
    #print("Cs results are: \n", results["lastPrice"])
    init_args = results.find_all(class_ = "page-title symbol-header-info")
    for elem in init_args:
        init_args = elem.get('data-ng-init')
    init_string = str(init_args)
    init_string = init_string[5:-1]
    dict_json = json.loads(init_string)
#    print(dict_json['lastPrice'])

    current_val = float(dict_json['lastPrice'])

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    crack_spread_obj = realInterestRate(dt_string)
    crack_spread_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the crack spread completed on {dt_string}. The crack spread value is: {crack_spread_obj.value}")
    return crack_spread_obj

#function that parses for crack spread value, and generates a new object with current value. WOrks, but uses selenium
def parseForCrackSpreadSelenium():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.minimize_window()
    URL = "https://en.macromicro.me/collections/19/mm-oil-price/4376/crude-oil-cracking-spread-vs-wti"
    #try 3 times the parsing:                
    try:
        driver.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing with selenium for the crack spread failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            driver.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the crack spread failed for the second time. Trying again...")
            time.sleep(10)
            try:
                driver.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the crack spread failed 3 times. Trying to return for a next loop.")
                return
    
    elem = driver.find_element(By.CLASS_NAME,"val")
    current_val = float(elem.text)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    crackspread_obj = CrackSpread(dt_string)
    crackspread_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the crack spread with selenium completed on {dt_string}. The new saved crack spread value: {crackspread_obj.value}")
    return crackspread_obj

#M2 Money Supply
def getM2MS(m2ms_list):
    current_m2ms_obj = parseForM2MS()
    m2ms_list.append(current_m2ms_obj)
    print(f"New value appended. The ffr obj list is now {len(m2ms_list)} elem long.")

def parseForM2MS():
    URL = "https://fred.stlouisfed.org/series/M2SL"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the M2 money supply failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the M2 money supply failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the M2 money supply failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    results = soup.find(id="meta-left-col")
    values = results.find(class_ = "series-meta-observation-value")
    values = values.text
    values = values.replace(",","")
    current_val = float(values)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    m2ms_obj = M2MoneySupply(dt_string)
    m2ms_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the M2 money supply rate completed on {dt_string}. The new saved money supply value: {m2ms_obj.value}")
    return m2ms_obj

#new homes sales
def getNewHomesSales(new_homes_list):
    new_homes_obj = parseForNewHomesSales()
    new_homes_list.append(new_homes_obj)
    print(f"New value appended. The new homes obj list is now {len(new_homes_list)} elem long.")

def parseForNewHomesSales():
    URL = "https://fred.stlouisfed.org/series/HSN1F"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the new homes sales failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the new homes sales failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the new homes sales failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    results = soup.find(id="meta-left-col")
    values = results.find(class_ = "series-meta-observation-value")
    values = values.text
    current_val = float(values)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    new_hs_obj = M2MoneySupply(dt_string)
    new_hs_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the new homes sales completed on {dt_string}. The new saved new homes sales value: {new_hs_obj.value}")
    return new_hs_obj

#Real Interest Rate
def getRealInterestRate(real_interest_rate_list):
    real_interest_rate_obj = parseForRealInterestRate()
    real_interest_rate_list.append(real_interest_rate_obj)
    print(f"New value appended. The new homes obj list is now {len(real_interest_rate_list)} elem long.")

def parseForRealInterestRate():
    URL = "https://fred.stlouisfed.org/series/REAINTRATREARAT10Y"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the real interest rate failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the real interest rate failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the real interest rate failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    results = soup.find(id="meta-left-col")
    values = results.find(class_ = "series-meta-observation-value")
    values = values.text
    current_val = float(values)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    real_interest_rate_obj = realInterestRate(dt_string)
    real_interest_rate_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the real interest rate completed on {dt_string}. The real interest rate value: {real_interest_rate_obj.value}")
    return real_interest_rate_obj

#federak fund rate
def getfederalFundRate(ffrate_list):
    current_ffr_obj = parseForFFR()
    ffrate_list.append(current_ffr_obj)
    print(f"New value appended. The ffr obj list is now {len(ffrate_list)} elem long.")

def parseForFFR():
    URL = "https://fred.stlouisfed.org/series/FEDFUNDS"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the federal fund rate failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the federal fund rate failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the federal fund rate failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    #results = soup.find(class_="series-meta-observation-value")
    results = soup.find(id="meta-left-col")
    #print("parsing for FFr results:",results)
    values = results.find(class_ = "series-meta-observation-value")
    #for elem in values:
    #    with open('parsed_ffr_class_search_results.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(elem)
    #with open('parsed_soup_results.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(results.prettify())
    current_val = float(values.text)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    ffr_obj = FFRate(dt_string)
    ffr_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the federal fund rate completed on {dt_string}. The new saved federal fund rate value: {ffr_obj.value}")
    return ffr_obj

#function that fetches fertility rate object, and appends it to the list
def getFertilityRate(fertility_rate_obj_list):
    current_fertility_obj = parseForFertilityRate()
    fertility_rate_obj_list.append(current_fertility_obj)
    print(f"New value appended. The fertility obj list is now {len(fertility_rate_obj_list)} elem long.")

def parseForFertilityRate():
    URL = "https://fred.stlouisfed.org/series/SPDYNTFRTINUSA"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the fertility rate failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the fertility rate failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the fertility rate failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    results = soup.find(id="meta-left-col")
    values = results.find(class_ = "series-meta-observation-value")

    current_val = float(values.text)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    fertility_obj = FertilityRate(dt_string)
    fertility_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the fertility rate completed on {dt_string}. The new saved fertility rate value: {fertility_obj.value}")
    return fertility_obj

#function that fetches  libor object, and appends it to the list
def getLibor(libor_obj_list):
    libor_obj = parseForLibor()
    libor_obj_list.append(libor_obj)
    print(f"New value appended. The libor obj list is now {len(libor_obj_list)} elem long.")

def parseForLibor():
    URL = "https://www.marketwatch.com/investing/interestrate/liborusd1m?countrycode=mr"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the libor failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the libor failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the libor failed 3 times. Trying to return for a next loop.")
                return
    
    soup = BeautifulSoup(page.content, "html.parser")
    
    results = soup.find(id="maincontent")
    #with open('parsed_webpage.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(results.prettify)
    values = results.find(class_ = "value")
    current_val = float(values.text)

    now =datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    libor_obj = Libor(dt_string)
    libor_obj.value = current_val

    with open('indicator_bot_log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"Parsing for the libor rate completed on {dt_string}. The new saved libor value: {libor_obj.value}")
    return libor_obj

def dummy():
    URL = "https://en.macromicro.me/collections/19/mm-oil-price/4376/crude-oil-cracking-spread-vs-wti"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('indicator_bot_log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing for the crack spread failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('indicator_bot_log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing for the crack spread failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('indicator_bot_log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing for the crack spread failed 3 times. Trying to return for a next loop.")
                return
    
    #with open('parsed_page.txt', 'w') as f:
    #    with redirect_stdout(f):
    #        print(page.text)

    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.minimize_window()
    driver.get(URL)
    #elem = driver.find_element(By.CLASS_NAME("val")).getText() #  .cssSelector("stat-val"));
    elem = driver.find_element(By.CLASS_NAME,"val")
    #result = elem.get_attribute("text")
    print("the element prints:", elem.text)
    #result = elem.text
    #print(result)
    #elem = driver.find_element(By.NAME, "stat-val")
    #for i in elem:
    #    print(i)
    
    #soup = BeautifulSoup(page.content, "html.parser")
    #results = soup.find(id="ccApp")
    #with open('parsed_webpage.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(page.text)
    #with open('parsed_soup_results.txt', 'w') as f:
    #        with redirect_stdout(f):
    #            print(results.prettify())

    #find the current value by class
    #current_val = results.find_all("div", class_="stat_val")
    #for elem in current_val:
    #    print("current val is:", elem)

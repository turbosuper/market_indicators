from flask import Blueprint, render_template
import os

import indicatorparsing

views = Blueprint(__name__, "views")

@views.route('/')
def index():
    #crack spread -every day
    dataCS = getIndicatorValuesToAList(indicatorparsing.crackspread_list)
    CSlabels = [row[0] for row in dataCS]
    CSvalues = [row[1] for row in dataCS]
    last_CS_index = len(indicatorparsing.crackspread_list) -1 
    if (last_CS_index) >= 1:            #at least two values have been saved. Proceed
        current_CS_val = indicatorparsing.crackspread_list[last_CS_index].value
        current_CS_date = indicatorparsing.crackspread_list[last_CS_index].fetched_time
        prev_CS_val = indicatorparsing.crackspread_list[last_CS_index-1].value
        prev_CS_date = indicatorparsing.crackspread_list[last_CS_index-1].fetched_time
        CStrend = getTrend(indicatorparsing.crackspread_list)
    else:
        current_CS_val = 0
        current_CS_date = "NA"
        prev_CS_val = 0
        prev_CS_date = "NA"
        CStrend = "NA"
    
    #Libor- every day
    datalibor = getIndicatorValuesToAList(indicatorparsing.libor_list)
    liborLabels = [row[0] for row in datalibor]
    liborValues = [row[1] for row in datalibor]
    last_libor_index = len(indicatorparsing.libor_list) -1 
    if (last_libor_index) >= 1:      
        current_libor_val = indicatorparsing.libor_list[last_libor_index].value
        current_libor_date = indicatorparsing.libor_list[last_libor_index].fetched_time
        prev_libor_val = indicatorparsing.libor_list[last_libor_index-1].value
        prev_libor_date = indicatorparsing.libor_list[last_libor_index-1].fetched_time
        libortrend = getTrend(indicatorparsing.libor_list)  
    else: 
        current_libor_val = 0
        current_libor_date = "NA"
        prev_libor_val = 0
        prev_libor_date = "NA"
        libortrend = "NA"

    #M2 Money Supply - every month
    datam2MS = getIndicatorValuesToAList(indicatorparsing.m2MS_list)
    m2MSLabels = [row[0] for row in datam2MS]
    m2MSValues = [row[1] for row in datam2MS]
    last_m2MS_index = len(indicatorparsing.m2MS_list) -1 
    if (last_m2MS_index) >= 1:      
        current_m2MS_val = indicatorparsing.m2MS_list[last_m2MS_index].value
        current_m2MS_date = indicatorparsing.m2MS_list[last_m2MS_index].fetched_time
        prev_m2MS_val = indicatorparsing.m2MS_list[last_m2MS_index-1].value
        prev_m2MS_date = indicatorparsing.m2MS_list[last_m2MS_index-1].fetched_time
        m2MStrend = getTrend(indicatorparsing.m2MS_list)  
    else: 
        current_m2MS_val = 0
        current_m2MS_date = "NA"
        prev_m2MS_val = 0
        prev_m2MS_date = "NA"
        m2MStrend = "NA"
    
    #new homes sales - every month
    datanew_homes_sales = getIndicatorValuesToAList(indicatorparsing.new_homes_sales_list)
    new_homes_salesLabels = [row[0] for row in datanew_homes_sales]
    new_homes_salesValues = [row[1] for row in datanew_homes_sales]
    last_new_homes_sales_index = len(indicatorparsing.new_homes_sales_list) -1 
    if (last_new_homes_sales_index) >= 1:      
        current_new_homes_sales_val = indicatorparsing.new_homes_sales_list[last_new_homes_sales_index].value
        current_new_homes_sales_date = indicatorparsing.new_homes_sales_list[last_new_homes_sales_index].fetched_time
        prev_new_homes_sales_val = indicatorparsing.new_homes_sales_list[last_new_homes_sales_index-1].value
        prev_new_homes_sales_date = indicatorparsing.new_homes_sales_list[last_new_homes_sales_index-1].fetched_time
        new_homes_salestrend = getTrend(indicatorparsing.new_homes_sales_list)  
    else: 
        current_new_homes_sales_val = 0
        current_new_homes_sales_date = "NA"
        prev_new_homes_sales_val = 0
        prev_new_homes_sales_date = "NA"
        new_homes_salestrend = "NA"

    #real interest rate - every month
    datareal_interest_rate = getIndicatorValuesToAList(indicatorparsing.real_interest_rate_list)
    real_interest_rateLabels = [row[0] for row in datareal_interest_rate]
    real_interest_rateValues = [row[1] for row in datareal_interest_rate]
    last_real_interest_rate_index = len(indicatorparsing.real_interest_rate_list) -1 
    if (last_real_interest_rate_index) >= 1:      
        current_real_interest_rate_val = indicatorparsing.real_interest_rate_list[last_real_interest_rate_index].value
        current_real_interest_rate_date = indicatorparsing.real_interest_rate_list[last_real_interest_rate_index].fetched_time
        prev_real_interest_rate_val = indicatorparsing.real_interest_rate_list[last_real_interest_rate_index-1].value
        prev_real_interest_rate_date = indicatorparsing.real_interest_rate_list[last_real_interest_rate_index-1].fetched_time
        real_interest_ratetrend = getTrend(indicatorparsing.real_interest_rate_list)  
    else: 
        current_real_interest_rate_val = 0
        current_real_interest_rate_date = "NA"
        prev_real_interest_rate_val = 0
        prev_real_interest_rate_date = "NA"
        real_interest_ratetrend = "NA"

    #Federal Fund Reserve - every month
    dataFFR = getIndicatorValuesToAList(indicatorparsing.ffrate_list)
    FFRlabels = [row[0] for row in dataFFR]
    FFRvalues = [row[1] for row in dataFFR]
    last_FFR_index = len(indicatorparsing.ffrate_list) -1 
    if (last_FFR_index) >= 1:      
        current_FFR_val = indicatorparsing.ffrate_list[last_FFR_index].value
        current_FFR_date = indicatorparsing.ffrate_list[last_FFR_index].fetched_time
        prev_FFR_val = indicatorparsing.ffrate_list[last_FFR_index-1].value
        prev_FFR_date = indicatorparsing.ffrate_list[last_FFR_index-1].fetched_time
        FFRtrend = getTrend(indicatorparsing.ffrate_list)  
    else: 
        current_FFR_val = 0
        current_FFR_date = "NA"
        prev_FFR_val = 0
        prev_FFR_date = "NA"
        FFRtrend = "NA"
    
    #Fertility Rate - every year
    dataFertility = getIndicatorValuesToAList(indicatorparsing.fertility_rate_list)
    fertilityLabels = [row[0] for row in dataFertility]
    fertilityValues = [row[1] for row in dataFertility]
    last_fertility_index = len(indicatorparsing.fertility_rate_list) -1 
    if (last_fertility_index) >= 1:      
        current_fertility_val = indicatorparsing.fertility_rate_list[last_FFR_index].value
        current_fertility_date = indicatorparsing.fertility_rate_list[last_FFR_index].fetched_time
        prev_fertility_val = indicatorparsing.fertility_rate_list[last_FFR_index-1].value
        prev_fertility_date = indicatorparsing.fertility_rate_list[last_FFR_index-1].fetched_time
        fertilitytrend = getTrend(indicatorparsing.fertility_rate_list)  
    else: 
        current_fertility_val = 0
        current_fertility_date = "NA"
        prev_fertility_val = 0
        prev_fertility_date = "NA"
        fertilitytrend = "NA"
 
    #generate the html
    return render_template('index.html',CSlabels=CSlabels, CSvalues=CSvalues, current_CS_val = current_CS_val, current_CS_date = current_CS_date, 
                            prev_CS_val = prev_CS_val, prev_CS_date = prev_CS_date, CStrend = CStrend, 
                            liborLabels=liborLabels, liborValues=liborValues, current_libor_val = current_libor_val, current_libor_date = current_libor_date, 
                            prev_libor_val = prev_libor_val, prev_libor_date = prev_libor_date, libortrend = libortrend,
                            m2MSLabels=m2MSLabels, m2MSValues=m2MSValues, current_m2MS_val = current_m2MS_val, current_m2MS_date = current_m2MS_date, 
                            prev_m2MS_val = prev_m2MS_val, prev_m2MS_date = prev_m2MS_date, m2MStrend = m2MStrend,
                            new_homes_salesLabels=new_homes_salesLabels, new_homes_salesValues=new_homes_salesValues, current_new_homes_sales_val = current_new_homes_sales_val, current_new_homes_sales_date = current_new_homes_sales_date, 
                            prev_new_homes_sales_val = prev_new_homes_sales_val, prev_new_homes_sales_date = prev_new_homes_sales_date, new_homes_salestrend = new_homes_salestrend,
                            real_interest_rateLabels=real_interest_rateLabels, real_interest_rateValues=real_interest_rateValues, current_real_interest_rate_val = current_real_interest_rate_val, current_real_interest_rate_date = current_real_interest_rate_date, 
                            prev_real_interest_rate_val = prev_real_interest_rate_val, prev_real_interest_rate_date = prev_real_interest_rate_date, real_interest_ratetrend = real_interest_ratetrend,
                            FFRlabels=FFRlabels, FFRvalues=FFRvalues, current_FFR_val = current_FFR_val, current_FFR_date = current_FFR_date, 
                            prev_FFR_val = prev_FFR_val, prev_FFR_date = prev_FFR_date, FFRtrend = FFRtrend,
                            fertilityLabels=fertilityLabels, fertilityValues=fertilityValues, current_fertility_val = current_fertility_val, current_fertility_date = current_fertility_date, 
                            prev_fertility_val = prev_fertility_val, prev_fertility_date = prev_fertility_date, fertilitytrend = fertilitytrend)

@views.route('/flats')
def show_flats_bot():
    file_stats = os.stat('flat_bot_log.txt')
    size_string = str(file_stats.st_size)
    file_contents = getLinesFromFile("flat_bot_log.txt")
    return render_template('flats.html', size=size_string, file_contents = file_contents)

@views.route('/indicators')
def show_indicator_bot():
    file_stats = os.stat('indicator_bot_log.txt')
    size_string = str(file_stats.st_size)
    file_contents = getLinesFromFile("indicator_bot_log.txt")
    return render_template('indicator_bot.html', size=size_string, file_contents = file_contents)

def getLinesFromFile(filename):
    file_contents = ""
    with open(filename) as f:
        lines_from_file = f.readlines()
    f.close()
    for i in range(len(lines_from_file)):
        file_contents = file_contents+ ' ' + lines_from_file[i]        
        i+=1

    #return file_contents
    return lines_from_file 

#function that reads the list of objects and prepares their values and labels for a graph
def getIndicatorValuesToAList(list_with_indicator_objects):
    ready_list = []
    for i in range(len(list_with_indicator_objects)):
        ready_list.append((list_with_indicator_objects[i].fetched_time, list_with_indicator_objects[i].value))
        #print(f"from the loop: indicator value {list_with_indicator_obj[i].value} has been fetched {list_with_indicator_obj[i].fetched_time}")
    return ready_list

#function that reads the trend of the values. possible outcome: slighlty rising (up to 2% change in value), rising (up to 5% change), rising fast (more than 5% change)
def getTrend(list_with_indicator_objects):
    assesment = "" 
    last_index = len(list_with_indicator_objects)-1
    newest_val = list_with_indicator_objects[last_index].value
    prev_val = list_with_indicator_objects[last_index-1].value
    #calculations
    difference = newest_val - prev_val
    if difference < 0:
        if abs(difference) < (newest_val *0.02):
            assesment = "slightly falling"
        elif abs(difference) < (newest_val *0.05):
            assesment = "falling"
        else:
            assesment = "falling fast"
    elif difference > 0:
        if difference < (newest_val *0.02):
            assesment = "slightly rising"
        elif difference < (newest_val *0.05):
            assesment = "rising"
        else:
            assesment = "rising fast"
    else:
        assesment = "stable"
    
    return assesment